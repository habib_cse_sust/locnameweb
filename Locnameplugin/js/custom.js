  // var map ;
        $(function () {

            // $('.map_canvas').hide();
            $("#formmapper").formmapper({
                map: ".map_canvas",
                details: "form",
                updateCallback: showCallback, // nnnn
                markerOptions: {
                    draggable: true
                },
                mapOptions: {
                    zoom: 15,
                    minZoom: 2,
                    maxZoom: 20, //The maximum zoom level the user is allowed achieve with controls. Default: 20
                    scrollwheel: true,
                    panControl: true,
                    mapTypeId: "roadmap"
                }
            });

            function showCallback(geocodeResult, parsedGeocodeResult){
                $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
            }

            $("#formmapper").bind("geocode:dragged", function (event, latLng) {
                $("#formmapper").formmapper("find", latLng.lat() + "," + latLng.lng());
            });

            $("#find").click(function () {
                $("#formmapper").trigger("geocode");

            }).click();

            $("#formmapper").click(function () {
            }).click();

            $('#formmapper').popover({'trigger': 'focus'});
            $('#locnamemaper').popover({'trigger': 'focus'});

            $('input:disabled').val('Add location above');

        });
    // ============================================
        $(function () {
            $('#dataConfirmModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var title = button.data('title');
                var body = button.data('body');
                var href = button.data('href');
                var modal = $(this);
                modal.find('.modal-title').text(title);
                modal.find('.modal-body').text(body);
                $('#dataConfirmOK').attr('href', href);
            });
        });


        // function of locname autocomplete =========================

        $(document).ready(function() {
            var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
            $(function()

            {
                //$(function() {
                var auto =$("#locnamemaper").autocomplete({
                    //source: 'getplaces',
					 source: function( request, response ) {
						 var sea = request.term ;
                        $.ajax({
                            url: "http://api-v2.locname.com/api/v2/autocomplete",
                            dataType: "jsonp",
                            data: {
                                term: request.term
                            },
                            success: function( data ) {
                                response( data );
                            }
                        });
                    },

                    // heigh light the matched search
                    open: function(e, ui)
                    {

                        var acData = $(this).data('uiAutocomplete');
                        acData.menu
                                .element
                                .find('a')
                                .each(function() {
                                    var me = $(this);
                                    var regex = new RegExp(acData.term, "gi");
                                    me.html( me.text().replace(regex, function (matched) {
                                        return termTemplate.replace('%s', matched) ;

                                    }
                                    ) );
                                });

                        // css ==============================
                        acData.menu
                                .element
                                .find('a')
                                .each(function() {
                                    var me = $(this);
                                    me.addClass("ui-menu-item-icon").css( "background-image", "url(http://jquery.com//favicon.ico)" );
                                });
                        // Powered by ==============================



                    },

                    minLength:1 ,
                    select: function( event, ui ) {

                        showLocNamePlaceOnMap(  ui.item.late,
                                                ui.item.long ,
                                                ui.item.id,
                                                ui.item.country,
                                                ui.item.city,
                                                ui.item.street_name ,
                                                ui.item.street_number ,
                                                ui.item.postal_code,
                                                ui.item.state  );

                                         }
                });

                    // Powered by LocName ==============================
                    auto.data("ui-autocomplete")._renderMenu = function(ul, items) {
                       var self = this;
                        $.each(items, function(index, item) {
                           // self._renderItem(ul, item);
                            self._renderItemData( ul, item )
                        });
                        // Adder
                        ul.append("<div id=\"locname_logo\">powered by LocName</div>");
                    };


            });

        });


        // locname function showLocNamePlaceOnMap ==============
        function showLocNamePlaceOnMap(late, long, id, country, city, street_name, street_number, postal_code ,state)
        {
            var infoWindow = new google.maps.InfoWindow;

            var xx  = document.getElementsByClassName("locname_map_canvas") ;

            // var  map = new google.maps.Map(document.getElementById("map_canvas_locname"),
            var map = new google.maps.Map(xx[0] ,
                    // var  map = new google.maps.Map($('.map_canvas'),
                    {
                        center: new google.maps.LatLng
                        (
                                late,
                                long
                        ),
                        zoom: 18,
                        mapTypeId: 'roadmap'
                    });

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(late, long),
                map: map
            });

            // after click do this
            $('.add-on').val(''); // remove every data inserted in boxes by google Map and let locaname take it's space
            $('#place_id').val(id);
            $('#street_number').val(street_number).removeAttr("disabled", "disabled");
            $('#route').val(street_name).removeAttr("disabled", "disabled");
            $('#locality').val(city).removeAttr("disabled", "disabled");
            $('#administrative_area_level_1').val(state).removeAttr("disabled", "disabled");
            $('#postal_code').val(postal_code).removeAttr("disabled", "disabled");
            $('#country').val(country).removeAttr("disabled", "disabled");
            $('#lat').val(late).removeAttr("disabled", "disabled");
            $('#lng').val(long).removeAttr("disabled", "disabled");
           
        }
		
		
		
        // WHEN check box cliecked 
        $(document).ready(function () {
                    $('#checkbox_email').click(function () {
                        if ($('#checkbox_email').prop('checked')) {
                            $('#sender_email_div').show();
                        }
                        else{
                            $('#sender_email_div').hide();
							$('#sender_email').val('');
                        }

                    });
                }
        );
		
		// toggle defualt map and locname 
		
		$(document).ready(function () {
    $('#formmapper').click(function () {
        $(".locname_map_canvas").hide();
        $(".map_canvas").show();
    });
    });
	
		$(document).ready(function () {
    $('.find').click(function () {
        $(".locname_map_canvas").hide();
        $(".map_canvas").show();
    });
    });
		$(document).ready(function () {
    $('#formmapper').change(function () {
		$('#locnamemaper').val('');
    });
    });
	
		$(document).ready(function () {
    $('#locnamemaper').click(function () {
        $(".map_canvas").hide();
        $(".locname_map_canvas").show();
    });
    });
	
	$(document).ready(function () {
      $('#locnamemaper').change(function () {
		$('#formmapper').val('');
    });
    });
	

